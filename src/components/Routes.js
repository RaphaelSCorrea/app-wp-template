import React from 'react';
import { Router, Stack, Scene  } from 'react-native-router-flux';

import Home from './Home';
import Single from './Single'

const Routers = () => (
  <Router>
    <Stack key="root">
      <Scene key="Home" component={Home} title="Home" />
      <Scene key="Single" component={Single} title="Postagem" />
    </Stack>
  </Router>
);

export default Routers;
