import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import { BASE_URL } from './configs/configs';

import { 
  Container, 
  Loading,
  TitelSingle,
  ContainerSingle,
  Content
} from './styled';


const Normalize = (str) => {
  let newStr = str.replace(/<\/?[^>]+(>|$)/g,'').replace(/&#8230;/, '').replace( /&nbsp;/g ,'\n');
  
  return newStr;
}

export const getPost = async (postId) => {
  const URL = `${BASE_URL}wp/v2/posts?include${postId}`;
  try {
    
    const resp = await axios.get(URL);
    return resp.data;
  } catch (e) {
    throw e;
  }
};

class Single extends Component {
  state = {
    isLoading: true,
    post: []
  }

  async componentWillMount () {
    const post = await getPost(this.props.postId);
    this.setState({post, isLoading: false });
  }

  render () {
    const { isLoading, post } = this.state;

    if (isLoading) {
      return (
        <Container>
          <Loading>Carregando ...</Loading>
        </Container>
      );    
    }

    return (
      <ScrollView>
        <ContainerSingle> 
          <TitelSingle> {post[0].title.rendered} </TitelSingle>
          <Content> {Normalize(post[0].content.rendered)} </Content>
        </ContainerSingle>
      </ScrollView>
    );
  }
}


export default Single ;
