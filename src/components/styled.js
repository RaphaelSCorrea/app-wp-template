import styled from 'styled-components/native';


export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Loading = styled.Text`
  font-size: 22px;
  color: #222;
`;

export const Card = styled.View`
  width: 100%;
  padding: 16px;
  margin: 8px 0;
  border-radius: 0;
  box-shadow: 0 1px 2px rgba(0,0,0,.1);
  background: #fff;
  flex: 1;
`;

export const Title = styled.Text`
  color: #c4170c;
  font-size: 20px;
`;

export const SubTitle = styled.Text`
  color: #555;
  font-size: 16px;
  letter-spacing: -.32px;
  margin-top: 8px;
`;

export const Content = styled.Text`
  font-weight: 600;
  color: #333;
  letter-spacing: -.32px;
`;

export const ImageCard = styled.Image`
  width: 100%;
  height: 100px;
  margin: 10px 0;
`;

export const ReadMoreButton = styled.TouchableOpacity`
  width: 100%;
`;

export const ReadMoreText = styled.Text`
  color: #c4170c;
`;


export const ContainerSingle = styled.View`
  flex: 1;
  padding: 16px;
`;

export const TitelSingle = styled.Text`
  color: #c4170c;
  font-size: 25px;
  margin: 16px 0;
`;
