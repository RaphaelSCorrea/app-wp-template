import React, { Component } from 'react';
import axios from 'axios';
import { BASE_URL } from './configs/configs';
import { ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { 
  Container, 
  Loading, 
  Card,
  Title,
  ImageCard,
  SubTitle,
  ReadMoreText,
  ReadMoreButton
} from './styled';

export const getPost = async () => {
  const URL = `${BASE_URL}wp/v2/posts?_embed`;
  try {
    
    const resp = await axios.get(URL);
    return resp.data;
  } catch (e) {
    throw e;
  }
};

const Normalize = (str) => {
  let newStr = str.replace(/<\/?[^>]+(>|$)/g,'').replace(/&#8230;/,'');
  
  return `${newStr.slice(0, 100)}...`;
}


class Home extends Component {
  state = {
    isLoading: true,
    posts: []
  }

  async initialState () {
  }
  
  async componentWillMount() {
    const fetchData = await getPost();
    this.setState({posts: fetchData, isLoading: false });
  }

  renderPost() {
    const { posts } = this.state;
    
    return posts.map((post, i) => {
      return (
        <Card
          key={post.id}
        >  
          <Title> {post.title.rendered} </Title>
          <ImageCard source={{ uri: post['_embedded']['wp:featuredmedia'][0].source_url }} />
          <SubTitle> {Normalize(post.excerpt.rendered)} </SubTitle>
          <ReadMoreButton> 
            <ReadMoreText onPress={ () => Actions.Single({ postId: post.id }) }> Continue lendo </ReadMoreText> 
          </ReadMoreButton>
        </Card>
      );
    })
  }

  render () {
    
    const { isLoading } = this.state;

    if (isLoading) {
      return (
        <Container>
          <Loading>Carregando ...</Loading>
        </Container>
      );    
    }

    return (
      <ScrollView>
        <Container>
          {this.renderPost()}
        </Container>
      </ScrollView>
    );
  }
}

export default Home ;
