import React, {Fragment} from 'react';
import Routers from './src/components/Routes';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillUpdate is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
]);

const App = () => {
  return (
    <Fragment>
      <Routers />
    </Fragment>
  );
};


export default App;
